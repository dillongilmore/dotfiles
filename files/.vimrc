set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/syntastic'
Plugin 'Lokaltog/vim-distinguished'
Plugin 'keith/tmux.vim'
Plugin 'evidens/vim-twig'
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-markdown'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

colorscheme distinguished

syntax on
set expandtab
set shiftwidth=4
set tabstop=4
set number
set noswapfile
set ff=unix
set nowrap

" Custom theme changes
" hi Comment guibg=None ctermbg=None
hi ExtraWhitespace guibg=#000000 ctermbg=0

" Matches
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
let &colorcolumn="80,".join(range(120,999),",")

" Plugin settings
"
" Syntastic
let g:syntastic_c_include_dirs = [ "/usr/include/glib-2.0" ]

