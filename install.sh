#!/bin/bash

for f in $(pwd)/files/* $(pwd)/files/.*; do
    if [[ -d "$f" ]] || [[ -f "$f" ]]; then
        skip=0
        for exception in "." ".."; do
            if [[ "$(pwd)/files/$exception" = "$f" ]]; then
                skip=1
            fi
        done

        if [ $skip -eq 1 ]; then
            continue
        fi

        ln -sf "$f" $HOME/$(basename "$f")
    fi
done

for f in $(pwd)/config/*; do
    if [[ -d "$f" ]] || [[ -f "$f" ]]; then
        skip=0
        for exception in "." ".."; do
            if [[ "$(pwd)/config/$exception" = "$f" ]]; then
                skip=1
            fi
        done

        if [ $skip -eq 1 ]; then
            continue
        fi

        [[ -d "$f" ]] || mv "$f" "$f-$(date +%s)"
        ln -sf "$f" $HOME/.config/$(basename "$f")
    fi
done

sudo pacman -S \
    "vim"\
    "git"\
    "pass"\
    "ack"\
    "acpi"\
    "termite"\
    "tmux"\
    "i3"\
    "openssh"\
    "lightdm-gtk-greeter"\
    "xorg-server" \
    "intel-ucode" \
    "alsa-utils" \
    "iw" \
    "wpa_supplicant" \
    "dialog" \
    "base" \
    "base-devel"

[[ -d "$HOME/usr/src" ]] || mkdir -p $HOME/usr/src
[[ -d "$HOME/var/build" ]] || mkdir -p $HOME/var/build

cd $HOME/var/build
git clone https://aur.archlinux.org/cower.git
cd $HOME/var/build/cower
makepkg -sri

aur=(
    "firefox-aurora"
)

for a in $aur; do
    cd $HOME/var/build
    cower -d -f "$a"
    cd $HOME/var/build/$a
    makepkg -sri
done

cd $HOME

echo "done!"

